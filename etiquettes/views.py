from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.safestring import mark_safe

import json, locale, datetime, pytz

from .models import *
from .dimensions import edtDim

PARTICULARITE = "pc"
DATE_FORMAT   = "%d %B %Y"

@login_required
def index(request):
    ## on essaie de trouver la date dans les paramètres
    ## en cas d'échec on lit l'heure courante
    date=""
    locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
    paris_tz = pytz.timezone("Europe/Paris")
    try:
        date=request.GET.get("date", "")
        # on met l'heure à midi plutôt que zéro heure
        date=timezone.datetime.strptime(date, "%Y-%m-%d")
        date+=datetime.timedelta(hours=12)
        date=paris_tz.localize(date)
    except:
        date = datetime.datetime.now(paris_tz)

    caseTeteColonne=[]
    left=edtDim["htitre"]["left"]+edtDim["minispace"]
    for i in range(10): # 10 places pour des étiquettes d'étiquettes de colonne
        case={
            "top": edtDim["htitre"]["top"]+edtDim["minispace"],
            "left": left,
            "class": "column-case",
            "z_index": 1018,
            }
        caseTeteColonne.append(case)
        left+=edtDim["colWidth"]
    ## mise en place des étiquettes de têtes de colonnes
    ## et des fiches
    etiqTeteColonne, etiqNonPlacees, fiches = restore(date)
    heures=[]
    top=0;
    horaire=[]
    for h in range(8,19):
        horaire.append({"h": h, "top": (h-8)*edtDim["rowHeight"]*2})
    for h in horaire:
        for m in [0,30]:
            heures.append({
                "h": "{:02d}:{:02d}".format(h["h"],m),
                "top": top,
            })
            top+=edtDim["rowHeight"]
    colors=[ ## couleurs prédéfinies des étiquettes
        "#aaaaaa", "#ffaaaa", "#ffffaa", "#aaffaa",
        "#aaffff", "#aaaaff", "#ffaaff", "#e8e8e8",
    ]
    return render(request, "accueil.html", {
        "caseTeteColonne": caseTeteColonne,
        "etiqTeteColonne": etiqTeteColonne,
        "etiqNonPlacees": etiqNonPlacees,
        "heures": heures,
        "horaire": horaire,
        "profs": Prof.tous(),
        "salles": Salle.tous(),
        "classes": Classe.tous(),
        "colors": colors,
        "fiches": fiches,
        "date": date.strftime(DATE_FORMAT), # seulement le jour, pas l'heure
    })


def cssCalcule(request):
    # attention if faut renvoyer un en-tête MIME "/text/css"
    response = render(request, "edt-calcule.css", {
        "edtDim": edtDim,
    })
    response["Content-Type"] = "text/css; charset=utf-8"
    return response
                  
def jsCalcule(request):
    # attention if faut renvoyer un en-tête MIME "/text/css"
    response = render(request, "functions.js", {
        "edtDim": edtDim,
    })
    response["Content-Type"] = "text/javascript; charset=utf-8"
    return response

def staff_check(user):
    return user.is_staff

@user_passes_test(staff_check)
def adminEdt(request):
    """
    administration d'EDT, indépendante de l'administration Django
    """
    dialog_content=None # pas de dialogue par défaut
    if "submit_salles" in request.POST:
        dialog_content=Salle.submit(request.POST.get("salles",""))
    elif "submit_profs" in request.POST:
        dialog_content=Prof.submit(request.POST.get("profs",""))
    elif "submit_classes" in request.POST:
        dialog_content=Classe.submit(request.POST.get("classes",""))
    return render(request, "adminEdt.html", {
        "debug": "no debug yet",
        "salles": Salle.linearise(),
        "profs": Prof.linearise(),
        "classes": Classe.linearise(), 
        "dialog_content": dialog_content,
    })

def cree_etiquette(request):
    duree=int(request.GET.get("duree","0"))
    prof=Prof.objects.filter(pk=request.GET.get("prof","0")).first()
    nom=prof.nom if prof else ""
    pclasse=Classe.objects.filter(pk=request.GET.get("classe","0")).first()
    classe=pclasse.nom if pclasse else ""
    couleur=request.GET.get("couleur","white")
    if duree and nom and classe and couleur:
        return render(request, "cree_etiquette.html",{
            "edtDim": edtDim,
            "height": duree * edtDim["rowHeight"],
            "nom": nom,
            "classe": classe,
            "couleur": couleur,
            "profId": prof.pk,
            "classeId": pclasse.pk,
            "duree": duree,
        })
    else:
        return HttpResponse("")

def save(request):
    """
    enregistrement d'une page de journée
    """
    locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
    paris_tz = pytz.timezone("Europe/Paris")
    put_date=request.POST.get("date")
    date=datetime.datetime.strptime(put_date,DATE_FORMAT)
    # on met l'heure à midi plutôt que zéro heure
    date+=datetime.timedelta(hours=12)
    date=paris_tz.localize(date)
    pfiches=json.loads(request.POST.get("fiches"))
    fiches=[]
    for f in pfiches:
        fiches.append(Fiche(**f).to_json())
    jsonFiches="""[{l}]""".format(l=", ".join(fiches))
    journee=Journee(
        date=date,
        salles=request.POST.get("salles"),
        etiquettes=jsonFiches
    )
    journee.save()
    return HttpResponse("<p>Enregistrement effectué, à la date<br/>du {}</p>".format(put_date))
   
def restore(date):
    """
    Restaurations d'une journée en tenant compte d'une date
    Cela met en place les étiquettes de colonnes et la étiquettes
    @param date une date consciente du fuseau horaire, dont on garde
    l'information de journée, et dont on règle ensuite l'heure à midi heure
    de Paris
    """
    date=date.replace(hour=12, minute=0, second=0, microsecond=0)
    etiqTeteColonne=[]
    etiqNonPlacees=[]
    fiches=[]
    ficheDivs=[]
    journees=Journee.objects.filter(date=date).order_by('enregistrement_date')
    if not journees:
        ## mise en place des étiquettes par défaut si on ne restaure rien ##
        salles=Salle.tous()
        salles0 = [s for s in salles if PARTICULARITE in s.particularite]
        salles1 = [s for s in salles if PARTICULARITE not in s.particularite]
        left=edtDim["htitre"]["left"]+edtDim["minispace"]
        for s in salles0:
            etiq={
                "content": s.nom,
                "top": edtDim["htitre"]["top"]+edtDim["minispace"],
                "left": left,
                "class": "column-title",
                "z_index": 1028,
                }
            etiqTeteColonne.append(etiq)
            left+=edtDim["colWidth"]
        left=edtDim["htitre"]["left"] + edtDim["htitre"]["width"] + edtDim["minispace"]
        top = edtDim["htitre"]["top"] + edtDim["htitre"]["height"] + edtDim["minispace"]
        for s in salles1:
            etiq={
                "content": s.nom,
                "top": top,
                "left": left,
                "class": "column-title",
                "z_index": 1028,
                }
            etiqNonPlacees.append(etiq)
            top+=3*edtDim["minispace"]
            left+=edtDim["minispace"]
    else: # des journées enregistrées existent, on les utilise
        journee=journees.last() # on garde la dernière version
        salles=json.loads(journee.salles)
        fiches=json.loads(journee.etiquettes)
        ## on vérifie que tous les noms de salles existent, sion on les re-crée
        for n in salles.values():
            s,created=Salle.objects.get_or_create(nom=n)
        for numcol in salles:
            numcoli=int(numcol)
            etiq={
                "content": salles[numcol],
                "top": edtDim["ctitre"]["top"],
                "left": edtDim["ctitre"]["left"]+edtDim["colWidth"]*numcoli,
                "class": "column-title",
                "z_index": 1028,
                }
            etiqTeteColonne.append(etiq)
        salles1=[s for s in Salle.tous() if s.nom not in salles.values()]
        left=edtDim["htitre"]["left"] + edtDim["htitre"]["width"] + edtDim["minispace"]
        top = edtDim["htitre"]["top"] + edtDim["htitre"]["height"] + edtDim["minispace"]
        for s in salles1:
            etiq={
                "content": s.nom,
                "top": top,
                "left": left,
                "class": "column-title",
                "z_index": 1028,
                }
            etiqNonPlacees.append(etiq)
            top+=3*edtDim["minispace"]
            left+=edtDim["minispace"]
    for f in fiches:
        ## on vérifie que le prof et la classe existent toujours
        ## sinon on les re-crée
        prof, created = Prof.objects.get_or_create(nom=f["prof"])
        classe, created = Classe.objects.get_or_create(nom=f["classe"])
        height = f["duree"]*edtDim["rowHeight"]
        left = f["col"]*edtDim["colWidth"]+edtDim["rack"]["left"]
        top = f["lig"]*edtDim["rowHeight"]+edtDim["rack"]["top"]
        div=render_to_string("cree_etiquette.html",{
            "edtDim": edtDim,
            "height": height,
            "top": top,
            "left": left,
            "couleur": f["couleur"],
            "classe": f["classe"],
            "nom": f["prof"],
            "profId": prof.pk,
            "classeId": classe.pk,
            "duree": f["duree"],
        })
        ficheDivs.append(div)

    return etiqTeteColonne, etiqNonPlacees, ficheDivs

def maintenant(request):
    """
    Affichage des étiquettes à un date donnée, ou au moment de l'appel :
    - accessible à tous ;
    - la date est donnée dans le paramètre "date", par GET.
    """
    duree_h=5
    date=None
    fiches=[]
    salles=[]
    locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
    paris_tz = pytz.timezone("Europe/Paris")
    date_format = DATE_FORMAT + " %H:%M"
    if "date" in request.GET:
        date=datetime.datetime.strptime(
            request.GET.get("date"),
            date_format
        )
        date=paris_tz.localize(date)
    else:
        date=datetime.datetime.now(paris_tz)
    date_a_midi=date.replace(hour=12, minute=0, second=0, microsecond=0)
    ## récupération des données pour la date recherchée
    journees=Journee.objects.filter(date=date_a_midi).order_by('enregistrement_date')
    hdebut = 8                 # l'heure de début est huit heures
    hfin   = 19                # l'heure de fin est dix neuf heures
    hmin   = date.hour-2       # on affichera les deux heures précédentes
    hmin   = max(hmin,8);      # l'heure minimale est huit heures
    hmax   = hmin + duree_h    # onze lignes à afficher
    if hmax > hfin:
        hmax=hfin              # on finit au pus à l'heure de fin
        hmin = hmax-duree_h    # toujours onze lignes
    erreur=""
    if not journees:
        erreur="Erreur : pas de données pour cette journée"
    else:
        journee=journees.last()
        
        # on récupère les fiches ###############################
        fiches = json.loads(journee.etiquettes)
        # on ne conserve que les fiches qui tiennent sur les
        # lignes horaires à montrer
        intervalle_lignes=range(2*(hmin-hdebut), 2*(hmax-hdebut))
        fiches = [f for f in fiches if f["lig"] in intervalle_lignes or\
                  f["lig"]+f["duree"]-1 in intervalle_lignes]
        # on réordonne les fiches par numéro de ligne croisssant
        # pour les poser en "tuiles inversées" plus tard
        fiches=sorted(fiches, key=lambda f: f["lig"])
        
        # on récupère les en-têtes des colonnes ################
        salles = json.loads(journee.salles)
        # on repère les colonnes utilisées
        colonnes_utiles=set([f["col"] for f in fiches])
        # on ne conserve que les salles utiles
        # et on renumérote les salle avec des entiers
        salles = [(int(k),v) for k,v in salles.items() if int(k) in colonnes_utiles]
        # puis on les ordonne par salle, alphabétiquement
        salles = sorted(salles, key=lambda t: t[1])
        # enfin on extrait les index des salles après cette mise en ordre
        index_salle=[k for k,v in salles]
    for f in fiches:
        f["top"]=(f["lig"]+2*(hdebut-hmin))*edtDim["rowHeight"]
        try:
            f["left"]=index_salle.index(f["col"])* edtDim["colWidth"]
        except:
            ## colonne inxistante pour la fiche, on va la placer bien
            ## à droite de tout
            f["left"]=3000
        f["width"]=edtDim["colWidth"]
        f["height"]=f["duree"]*edtDim["rowHeight"]
    salleDivs=[{"nom": nom} for k, nom in salles]
    for i, s in enumerate(salleDivs):
        s["left"]=edtDim["ctitre"]["left"]+i*edtDim["colWidth"]
        s["top"]=edtDim["ctitre"]["top"]
    heures=[]
    top=2*(hdebut-hmin)*edtDim["rowHeight"]
    for h in range(8,19):
        for m in (0,30):
            heures.append({
                "nom": "{:02d}:{:02d}".format(h,m),
                "top": top,
            })
            top+=edtDim["rowHeight"]
    # le lieu des étiquettes
    rack={
        "top": edtDim["rack"]["top"],
        "left": edtDim["rack"]["left"],
        "width": len(salles)*edtDim["colWidth"],
        "height": 2*duree_h*edtDim["ctitre"]["top"],
    }
    # le lieu de l'URL
    hint={
        "top": rack["top"]+ rack["height"]+ edtDim["minispace"],
        "left": edtDim["timeslots"]["left"],
    }
    return render(request,"maintenant.html", {
        "erreur": erreur,
        "date": date.strftime("%A " + date_format),
        "fiches" : fiches,
        "salleDivs": salleDivs,
        "edtDim": edtDim,
        "heures": heures,
        "rack": rack,
        "hint": hint,
    })

def su_check(user):
    return user.is_superuser

@user_passes_test(su_check)
def importer(request):
    """
    importation d'une base de données de la version précédente d'EDT
    """
    import os
    import sqlite3
    dialog_content=""
    dbPath=""
    conn=None
    cur=None
    tables=set()
    if "db" in request.POST:
        dbPath=request.POST.get("db")
        try:
            conn= sqlite3.connect(dbPath)
            cur=conn.cursor()
            cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
            tables=set([row[0] for row in cur])
        except:
            pass
        if tables == {'users', 'classes', 'cols', 'draggables', 'noms'}:
            dialog_content=mark_safe(import1(cur))
        else:
            dialog_content="Fichier {} incorrect".format(dbPath)
    return render(request, "importer.html", {
        "dbPath" : dbPath,
        "dialog_content": dialog_content,
    })

def import1 (cursor):
    """
    importation des données d'une base anciennde d'EDT
    @param cursor un curseur sur la base de données
    @return un message au format HTML
    """
    #######################################################
    result="<h1>Import de classes</h1>"
    cursor.execute ("select * from classes")
    new=[]
    for row in cursor:
        classe, created = Classe.objects.get_or_create(nom=row[1])
        if created:
            new.append(classe.nom)
    if new:
        result+="<p>{}</p>".format(", ".join(new))

    #######################################################
    result+="<h1>Import de salles</h1>"
    cursor.execute ("select * from cols")
    new=[]
    for row in cursor:
        obj, created = Salle.objects.get_or_create(nom=row[0])
        if created:
            new.append(obj.nom)
    if new:
        result+="<p>{}</p>".format(", ".join(new))

    #######################################################
    result+="<h1>Import de profs</h1>"
    cursor.execute ("select * from noms")
    new=[]
    for row in cursor:
        obj, created = Prof.objects.get_or_create(nom=row[1])
        if created:
            new.append(obj.nom)
    if new:
        result+="<p>{}</p>".format(", ".join(new))
        
    #######################################################
    result+="<h1>Import de journées</h1>"
    cursor.execute ("select * from draggables")
    for row in cursor:
        ## récupération des tickets, on ajout des guillemets où ça manque
        tjson=re.sub(r'([a-zA-Z]+):',r'"\1":', row[1])
        ## et on corige l'encodage de certains accents
        tjson=re.sub(r'\\xE9',r'é', tjson)
        tjson=re.sub(r'\\xE7',r'ç', tjson)
        tickets=json.loads(tjson)

        ## récupération des salles
        ordering=json.loads(row[3])
        salles={}
        for i,s in enumerate(ordering):
            if s:
                salles[str(i)]=s

        ## récupération de la date
        locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
        paris_tz = pytz.timezone("Europe/Paris")
        # on met l'heure à midi plutôt que zéro heure
        date=timezone.datetime.strptime(row[4], "%Y-%m-%d")
        date+=datetime.timedelta(hours=12)
        date=paris_tz.localize(date)
        ## on convertit top et left en colonne et ligne
        """ EXTRAITS DU CODE DE ETIQUETTES.JS
        var rack ={
	  colwidth: 90,                // largeur des colonnes en pixels
	  linHeight: 50,               // hauteur des lignes en pixels
	  mainTop:   60,               // haut de l'affichage principal
	  mainLeft:  5,                // marge gauche de l'affichage principal
	  toolWidth: 200,              // largeur de la zone d'outils
	  toolHeight:64,               // hauteur de la zone d'outils
        };
        e.col = Math.floor(
	    (e.offset.left-rack.mainLeft-rack.toolWidth)/rack.colwidth)
	e.heure=8+Math.floor(
	    (e.offset.top-rack.mainTop-rack.toolHeight)/rack.linHeight
	)/2;        
        """
        ## réencodage des tickets
        fiches=[]
        new=[]
        for t in tickets:
            col=int((t["offset"]["left"]-5-200)/90)
            lig=int((t["offset"]["top"]-60-64)/50)
            fiches.append(Fiche(prof=t["nom"], classe=t["classe"], duree=t["duree"], couleur=t["color"], col=col, lig=lig))
        fiches=[f.to_json() for f in fiches]
        j, created = Journee.objects.get_or_create(
            date=date, salles=json.dumps(salles),
            etiquettes="[{}]".format(", ".join(fiches))
        )
        if created:
            new.append(date.strftime("%Y-%m-%d"))
        result+=", ".join(new)
    return result

def ping(request):
    """
    Simple service permettant de vérifier si le service répond
    """
    return HttpResponse("OK")
