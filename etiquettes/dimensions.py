"""
Constantes définissant les dimensions et positions de la page d'accueil
"""

colNum   = 10  # nombre de colonnes implémentée
colWidth = 90  # largeur d'une colonne
rowHeight = 50 # hauteur d'une ligne
minispace = 7  # petit espacement

edtDim ={
    "rowHeight": rowHeight,
    "colWidth" : colWidth,
    "minispace": minispace,
    # body
    "bodyHeight": 1300,
    # #horiz-titres
    "htitre": {
        "top": 60,
        "left": 210,
        "width":  colNum*colWidth,
        "height": 54,
    },
    # .column-titles pour ceux qui sont en haut
    "ctitre": {
        "top": 60 + minispace,
        "left": 210 + minispace,
    },
    # .timeslot
    "vtitre": {
        "left": 13,
        "height": int(rowHeight-minispace/7*5),
        "width": 160,
    }
}

edtDim["timeslots"] = {
    "top": edtDim["htitre"]["top"] + edtDim["htitre"]["height"] + minispace,
    "left": int(minispace/7*5),
    "width": edtDim["htitre"]["left"]-2*minispace,
    "height": rowHeight*2*11, # nombres d'heures
}

edtDim["rack"] = {
    "top": edtDim["htitre"]["top"] + edtDim["htitre"]["height"] + minispace,
    "left": edtDim["htitre"]["left"],
    "height": rowHeight*2*11, # nombres d'heures
    "width": colNum*colWidth,
}

edtDim["rack_div"] = {
    "height": rowHeight*2,
    "left": 0,
    "width": colNum*colWidth,
}
