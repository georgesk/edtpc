/**
 * fonctions calculées par un template
 **/

/**
 * fonction de rappel pour la création d'une étiquette
 * @param other une autre étiquette, éventuellement
 **/
function create(other){
    var d=$("#create_dialog");
    if (other){
	$("#roue").val(other.find("input.couleur").val());
	$("#select_prof").val(other.find("input.prof").val());
	$("#select_classe").val(other.find("input.classe").val());
	$("#select_duree").val(other.find("input.duree").val());
    }
    d.dialog(
	{
	    width: 640,
	    buttons: [
		{
		    text: "OK",
		    icon: "ui-icon-heart",
		    click: function() {
			$( this ).dialog( "close" );
			cree_etiquette(
			    $("#select_prof").val(),
			    $("#select_classe").val(),
			    $("#select_duree").val(),
			    $("#roue").val(),
			);
		    }
		},
	    ],
	},
    );
}

/**
 * création d'une nouvelle étiquette,
 * @param prof id de professeur
 * @param classe id de classe
 * @param duree durée du cours en demi-heures
 * @param couleur couleur à un format hexadécimal , par exemple #aaffff
 **/
function cree_etiquette(prof, classe, duree, couleur){
    $.ajax( {
	url: "/cree_etiquette",
	method: "get",
	data: {
	    prof: prof,
	    classe: classe,
	    couleur: couleur,
	    duree: duree
	},
    }).done(
	function(data){
	    if (data != ""){
		var d=$(data);
		d.draggable({
		    stop: pose_etiquette,
		    start: prend_etiquette,
		});
		$("body").append(d);
	    }
	}
    )
}

/**
 * fonction de rappel pour un titre qu'on commence à tirer
 **/
function prend_titre(event, ui){
    var d=$(ui.helper);
    d.removeClass("bottom");
    d.addClass("top");
}

/**
 * fonction de rappel pour un titre qu'on cesse de tirer
 **/
function pose_titre(event, ui){
    var d=$(ui.helper);
    /* rabaisse le titre */
    d.removeClass("top");
    d.addClass("bottom");
    /* accrochage au plus proche nœud de la grille */
    var pos=plusProcheTitre(ui.offset);
    d.animate(pos);
}

/**
 * fonction de rappel pour une étiquette qu'on commence à tirer
 **/
function prend_etiquette(event, ui){
    var d=$(ui.helper);
    d.removeClass("bottom");
    d.addClass("top");
}

/**
 * fonction de rappel pour une étiquette qu'on cesse de tirer
 **/
function pose_etiquette(event, ui){
    var d=$(ui.helper);
    /* rabaisse l'étiquette */
    d.removeClass("top");
    d.addClass("bottom");
    /* test de la table d'écriture, pour un clonage */
    if (lesBoitesSeRecouvrent (d,$("#create"))){
	d.animate(ui.originalPosition);
	create(d);
	return;
    }
    /* test de la poubelle, pour un effacement */
    if (lesBoitesSeRecouvrent (d, $("#delete"))){
	d.remove();
	return;
    }
    /* accrochage au plus proche nœud de la grille */
    var pos=plusProcheAncrage(ui.offset);
    d.animate(pos);
}

/**
 * renvoie la position d'ancrage la plus proche d'une étiquette
 * @param pos un objet {left, top}
 * @return un autre objet {left, top}
 **/
function plusProcheAncrage(pos){
    /* coordonnées par rapport au coin haut gauche du rack */
    var left=pos.left-{{edtDim.rack.left}};
    var top=pos.top-{{edtDim.rack.top}};
    if (left < 0) left=0;
    if (top < 0) top=0;
    var ncol = Math.round(left/{{edtDim.colWidth}});
    var nlig = Math.round(top/{{edtDim.rowHeight}});
    return {
	left: {{edtDim.rack.left}}+ncol*{{edtDim.colWidth}},
	top: {{edtDim.rack.top}}+nlig*{{edtDim.rowHeight}},
    }
}

/**
 * renvoie la position d'ancrage la plus proche d'une étiquette
 * @param pos un objet {left, top}
 * @return un autre objet {left, top}
 **/
function plusProcheTitre(pos){
    /* on ne bouge pas une étiquette qui est trop bas sous la barre */
    if (pos.top > 2* {{edtDim.ctitre.top}}) return pos;
    /* coordonnées par rapport au coin haut gauche de la barre des titres */
    var left=pos.left-{{edtDim.ctitre.left}};
    if (left < 0) left=0;
    var ncol = Math.round(left/{{edtDim.colWidth}});
    /* le cas échéant, on peut prolonger le cadre sous-jacent et le rack */
    etendColonnes(ncol);
    /* renvoie la nouvelle position */
    return {
	left: {{edtDim.ctitre.left}}+ncol*{{edtDim.colWidth}},
	top:  {{edtDim.ctitre.top}},
    }
}

/**
 * étend le nombre de colonnes quand il en faut plus
 **/
function etendColonnes(ncol){
    var largeur={{edtDim.colWidth}}*(ncol+1);
    var sous_titres = $("#horiz-titles");
    if (largeur > parseInt(sous_titres.css("width"))){
	/* seulement s'il faut plus de colonnes qu'il n'y en a déjà */
	sous_titres.css("width", largeur+"px");
	$("[id^='rack_']").css("width", largeur+"px");
	$(".column-title").each(function(){
	    el=$(this);
	    var left=parseInt(el.css("left"));
	    var top=parseInt(el.css("top"));
	    var inc={{edtDim.htitre.left}}+largeur-left;
	    if (top > {{edtDim.ctitre.top}} && inc > 0){
		inc=Math.ceil(inc/{{edtDim.colWidth}})*{{edtDim.colWidth}};
		el.css("left",(left+inc)+"px");
	    }
	})
    }
}

/**
 * détermine si deux bounding boxes se recouvrent
 * @param aObj un objet jQuery
 * @param bObj un objet jQuery
 **/
function lesBoitesSeRecouvrent (aObj, bObj){
    var a=aObj[0].getBoundingClientRect();
    var b=bObj[0].getBoundingClientRect();
    return (Math.abs(a.x - b.x) * 2 < (a.width + b.width)) &&
        (Math.abs(a.y - b.y) * 2 < (a.height + b.height));
}


/**
 * Injecte une couleur dans le contrôle minicolors attaché à #roue
 * la couleur est prise d'un objet dont on a la référence
 * @param jqObj un objet jQuery dont le style a un attribut background
 */
function setMinicolorsFromButton(jqObj){
    var color = jqObj.attr("style");
    var bgPattern = /.*background: ([^;]*).*/;
    var match=color.match(bgPattern);
    var color1=match[1];
    $("#roue").val(color1);
    $("#roue").minicolors('settings', {defaultValue: color1});
}

/**
 * fonction de rappel pour les changements de durée d'une étiquette à créer
 **/
function dureeChangee(event, ui){
    var val=parseInt(ui.value);
    var h=["00","01","02","03","04"];
    var m=["00","30"];
    var hm=h[Math.floor(val/2)]+":"+m[val%2];
    $("#d12").text(hm);
}

/**
 * fonction de rappel pour la sauvegarde
 **/
function save(){
    var date=$("#save_calendar").val()
    var salles=new Object();
    $(".column-title").each( function(){
	var d=$(this);
	if (parseInt(d.css("top"))=={{edtDim.ctitre.top}}){
	    /* on récupère toutes les salles des titres actifs */
	    var col=parseInt(
		(parseInt(d.css("left"))-{{edtDim.ctitre.left}}) / {{edtDim.colWidth}}
	    );
	    salles[col]=d.text().trim();
	}
    });
    var fiches=new Array();
    $(".etiquette").each(function(){
	var d=$(this);     // div de la fiche
	var p=d.find("p"); // les paragraphes
	fiches.push({
	    prof: $(p[1]).text().trim(),
	    classe: $(p[0]).text().trim(),
	    duree: parseInt(d.find("input.duree").val()),
	    couleur: d.find("input.couleur").val(),
	    left: parseInt(d.css("left")),
	    top: parseInt(d.css("top")),
	});
    })
    $.post("/save", {
	date: date,
	salles: JSON.stringify(salles),
	fiches: JSON.stringify(fiches),
	csrfmiddlewaretoken: $("input[name='csrfmiddlewaretoken']").val(),
    }).done(function(data){
	message(data);
    })
}

function message(msg){
    var d=$("#dialog");
    var fadeOut = function() {
	d.fadeOut("slow", function(){d.dialog("close")});
    }
    d.html(msg);
    d.dialog();
    setTimeout(fadeOut,5000);
}

/**
 * Fonction de rappel invoquée au clic sur une image d'étiquette
 * renvoie ver la page maintenant.php avec la bonne heure
 * @param timeslot une chaîne dont le format est "hh:mm"
 **/
function maintenant(timeslot){
    var url="/maintenant?date="+$("#save_calendar").val().replace(" ","+") + "+" + timeslot;
  window.open(url);
}



/******************************************************************
 * fonctions lancées au démarrage
 *****************************************************************/

$(function(){
    /*********** mise en place des widgets calendriers *************/
    $("#load_calendar").datepicker({
        dateFormat: "dd MM yy",
	timeFormat: "",
        autoSize: true,
        showButtonPanel: true,
    } );
    $("#load_calendar").change(function(){
      var d = ('0' + $(this).datepicker('getDate').getDate()).slice(-2);
      var m = ('0' + ($(this).datepicker('getDate').getMonth() + 1)).slice(-2);
      var y = $(this).datepicker('getDate').getFullYear();
      document.location.href="/?date="+y+"-"+m+"-"+d;
    });
    
    $("#save_calendar").datepicker({
        dateFormat: "dd MM yy",
        autoSize: true,
        showButtonPanel: true,
    } ); 
    $("#save_calendar").change(function(){
	message("<p>Pour le prochain enregistrement,<br/>la date du tableau sera le "+ $(this).val()+"</p>");
	var pageTitle="..:: EDT -> "+$(this).val()+" ::..";
	$(document).attr('title', pageTitle);
    });
    
    /******** lignes de script exécutées une fois au démarrage **********/
    $("#roue").minicolors({control: 'wheel', theme: 'default'});
    $("#select_duree").spinner({min: 1, max:8, spin: dureeChangee});
    $(".column-title").draggable({stop: pose_titre, start: prend_titre,});
    $(".etiquette").draggable({stop: pose_etiquette, start: prend_etiquette,});
    $(".column-title").each(function(){
	var d =$(this);
	if (parseInt(d.css("top"))=={{edtDim.ctitre.top}}){
	    // pour les titres de colonne placés dans la barre
	    // on prolonge la barre et le rack si nécessaire
	    var left=parseInt(d.css("left"))-{{edtDim.ctitre.left}};
	    var ncol = Math.round(left/{{edtDim.colWidth}});
	    if (ncol >= 10) etendColonnes(ncol);
	}
    })
});
