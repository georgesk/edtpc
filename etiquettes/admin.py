from django.contrib import admin

# Register your models here.
from .models import *

class SalleAdmin(admin.ModelAdmin):
    fields = ('nom', 'particularite', 'capacite')
    list_display = ('nom', 'particularite', 'capacite')
    ordering = ('nom',)


admin.site.register(Salle, SalleAdmin)
