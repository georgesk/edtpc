from django.db import models
from django.utils.safestring import mark_safe

import re

from .dimensions import edtDim

class Salle(models.Model):
    nom = models.CharField(max_length=10, unique=True)
    particularite = models.CharField(max_length=100, default="", blank=True)
    capacite = models.IntegerField(default=0)

    def __str__(self):
        return self.nom

    @staticmethod
    def tous():
        """
        Renvoie la liste d'un queryset, ordonnée alphabétiquement sans tenir
        compte de la casse.
        """
        return sorted(list(Salle.objects.all()), key=lambda x: str(x.nom).lower())
    
    @staticmethod
    def linearise():
        """
        renvoie une version linéarisée des salles
        """
        salles=Salle.tous()
        return "\n".join(["{} : {} : {}".format(s.nom, s.particularite, s.capacite) for s in salles])

    @staticmethod
    def submit(salles):
        """
        traitement du contenu du TEXTAREA concernant les salles
        @param salles le contenu du textarea
        @return le code HTML d'un dialogue d'acquittement de tâche
        """
        salles=salles.split("\n")
        pattern=r"^([^:]*):([^:]*):([^:]*)$"
        traitees=[]
        modifiees=[]
        nouvelles=[]
        for s in salles:
            m=re.match(pattern, s)
            if m and m.group(3).strip().isnumeric():
                nom=m.group(1).strip()
                particularite=m.group(2).strip()
                capacite=int(m.group(3).strip())
                trouve=Salle.objects.filter(nom=nom).first()
                if trouve:
                    # ne modifie la salle que si celle-ci est différente
                    if trouve.capacite != capacite or \
                       trouve.particularite != particularite:
                        trouve.capacite = capacite
                        trouve.particularite = particularite
                        trouve.save()
                        traitees.append(trouve)
                        modifiees.append(trouve)
                    else: # salle nom modifiée
                        traitees.append(trouve)
                else: # pas de salle connue sous le même nom
                    ns=Salle(nom=nom, particularite=particularite, capacite=capacite)
                    ns.save()
                    traitees.append(ns)
                    nouvelles.append(ns)
        if len(traitees): # si au moins une salle a été reconnue et traitée
            asupprimer=[s for s in Salle.objects.all() if s.nom not in [t.nom for t in traitees]]
            if asupprimer:
                for s in asupprimer:
                    s.delete()
        message="<p>{} salles traitée(s)</p>".format(len(traitees))
        message+=listeTitree("Nouvelles salles", nouvelles)
        message+=listeTitree("Salles modifiées", modifiees)
        message+=listeTitree("Salles supprimées", asupprimer)

        return mark_safe(message)


class Prof(models.Model):
    nom = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.nom;

    @staticmethod
    def tous():
        """
        Renvoie la liste d'un queryset, ordonnée alphabétiquement sans tenir
        compte de la casse.
        """
        return sorted(list(Prof.objects.all()),
                      key=lambda x: str(x.nom).lower())
    
    @staticmethod
    def linearise():
        """
        renvoie une version linéarisée des profs
        """
        return "\n".join([str(p) for p in Prof.tous()])

    @staticmethod
    def submit(profs):
        """
        traitement du contenu du TEXTAREA concernant les profs
        @param profs le contenu du textarea
        @return le code HTML d'un dialogue d'acquittement de tâche
        """
        profs=profs.split("\n")
        traitees=[]
        nouvelles=[]
        for p in profs:
            if p:
                trouve=Prof.objects.filter(nom=p).first()
                if not trouve:
                    np=Prof(nom=p)
                    np.save()
                    nouvelles.append(np)
                    traitees.append(np)
                else:
                    traitees.append(trouve)

        if len(traitees): # si au moins un prof a été reconnu et traité
            asupprimer=[p for p in Prof.objects.all() if p.nom not in [t.nom for t in traitees]]
            if asupprimer:
                for p in asupprimer:
                    p.delete()
        message="<p>{} profs traité(es)</p>".format(len(traitees))
        message+=listeTitree("Nouveaux profs", nouvelles)
        message+=listeTitree("Profs effacés", asupprimer)

        return mark_safe(message)

class Classe(models.Model):
    nom = models.CharField(max_length=10, unique=True)
    effectif = models.IntegerField(default=0)

    def __str__(self):
        return self.nom;

    @staticmethod
    def tous():
        """
        Renvoie la liste d'un queryset, ordonnée alphabétiquement sans tenir
        compte de la casse.
        """
        return sorted(list(Classe.objects.all()), key=lambda x: str(x.nom).lower())
    
    @staticmethod
    def linearise():
        """
        renvoie une version linéarisée des classes
        """
        classes=Classe.tous()
        return "\n".join(["{} : {}".format(c.nom, c.effectif) for c in classes])
    
    @staticmethod
    def submit(classes):
        """
        traitement du contenu du TEXTAREA concernant les classes
        @param classes le contenu du textarea
        @return le code HTML d'un dialogue d'acquittement de tâche
        """
        classes=classes.split("\n")
        pattern=r"^([^:]*):([^:]*)$"
        traitees=[]
        modifiees=[]
        nouvelles=[]
        asupprimer=[]
        for c in classes:
            m=re.match(pattern, c)
            if m and m.group(2).strip().isnumeric():
                nom=m.group(1).strip()
                effectif=int(m.group(2).strip())
                trouve=Classe.objects.filter(nom=nom).first()
                if trouve:
                    # ne modifie la classe que si celle-ci est différente
                    if trouve.effectif != effectif:
                        trouve.effectif = effectif
                        trouve.save()
                        traitees.append(trouve)
                        modifiees.append(trouve)
                    else: # classe nom modifiée
                        traitees.append(trouve)
                else: # pas de classe connue sous le même nom
                    ns=Classe(nom=nom, effectif=effectif)
                    ns.save()
                    traitees.append(ns)
                    nouvelles.append(ns)
        if len(traitees): # si au moins une classe a été reconnue et traitée
            asupprimer=[s for s in Classe.objects.all() if s.nom not in [t.nom for t in traitees]]
            if asupprimer:
                for s in asupprimer:
                    s.delete()
        message="<p>{} classes traitée(s)</p>".format(len(traitees))
        message+=listeTitree("Nouvelles classes", nouvelles)
        message+=listeTitree("Classes modifiées", modifiees)
        message+=listeTitree("Classes supprimées", asupprimer)

        return mark_safe(message)

def listeTitree(titre, salles):
    """
    @param titre : le nom de la liste de salles
    @param salles un itérable d'objets Salle
    @return du code HTML pur afficher la liste de salles
    """
    if not salles:
        return ""
    result="{} :<ul>".format(titre)
    for s in salles:
        result+="<li>{}</li>".format(s.nom)
    result+="</ul>"
    return result

class Journee(models.Model):
    date                = models.DateTimeField()# la date nominale
    enregistrement_date = models.DateTimeField( # le moment de l'enregistrement
        auto_now_add=True, blank=True)
    salles              = models.TextField()    # liste de salles, (JSON)
    etiquettes          = models.TextField()    # liste de fiches, (JSON)

    def __str__(self):
        return "Enregistrement de la journée {}".format(
            self.enregistrement_date
        )
    
class Fiche:
    """
    Objet Fiche, qui contient les données de la fiche (pas d'index, au cas
    où des noms de profs ou de salles veanient à disparaître de la base),
    mais où cependant les numéros de colonne et de ligne sont calculés
    de façon à ne pas dépendre du module dimensions.py
    """

    def __init__(self, prof="", classe="", duree=1, couleur="#ffaaaa",
                 col=0, lig=0, left=None, top=None):
        """
        Le constructeur
        @param prof nom du prof
        @param classe nom de la classe
        @param duree durée du cours en nombre de tranches horaires
        @param couleur la couleur en format hexa
        @param col numéro de colonne (index dans la liste des classes du jour)
        @param lig numéro de ligne (une par tranche horaire, d'une 1/2 heure)
        @param left décalage ver la gauche de la fiche dans la fenêtre
        @param top décalage vers le bas de la fiche dans la fenêtre
        """
        self.col=col
        self.lig=lig
        self.prof=prof
        self.classe=classe
        self.duree=duree
        self.couleur=couleur
        if left:
            self.col=(left-edtDim["rack"]["left"])//edtDim["colWidth"]
        if top:
            self.lig=(top-edtDim["rack"]["top"])//edtDim["rowHeight"]
        return
    
    def __str__(self):
        return "Fiche : <<{prof}, {classe}, {duree}, {couleur}, {col}, {lig}>>".format(**self.__dict__)

    def __repr__(self):
        return str(self)

    def to_json(self):
        return """{{"prof": "{prof}", "classe": "{classe}", "duree": {duree}, "couleur": "{couleur}", "col": {col}, "lig": {lig} }}""".format(**self.__dict__)
    
