from django.apps import AppConfig


class EtiquettesConfig(AppConfig):
    name = 'etiquettes'
