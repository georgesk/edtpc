#!/usr/bin/python3
"""
Un afficheur dédié qui lit les données de l'emploi du temps et les
affiche dans une fenêtre graphique adpatée à l'écran disponible.

Les données à afficher sont relatives à une date et une heure.
Doivent être affichées les données du jour, depuis H-1 jusqu'à H+3,
si on demande un affichage à l'heure H (soit : cinq lignes de données)
"""
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebKit import *
from PyQt5.QtWebKitWidgets import *
from PyQt5.QtNetwork import *

import sys, argparse, textwrap, atexit, re, datetime

from Ui_afficheur import Ui_MainWindow

class MaFenetre(QMainWindow):
    # variables statiques de la classe
    proxyHost=None
    proxyPort=3128

    def __init__(self, parsed, parent=None):
        """
        Le constructeur
        @param parsed le résultat de l'examen des paramètres de la ligne
        de commande
        "date", "proxy" et "port"
        """
        QMainWindow.__init__(self, parent)
        self.ui=Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.menubar.hide()
        self.ui.toolButton.clicked.connect(self.close)
        self.ui.label.setText("affectation des salles de physique-chimie")
        self.url=parsed.url
        self.date=None
        if len(parsed.date)>0:
            datePattern=re.compile(r"(\d\d\d\d).(\d\d).(\d\d).(\d\d).(\d\d)")
            m= datePattern.match(parsed.date)
            if m:
                date="{0:s} {1:s} {2:s} {3:s}:{4:s}".format(*m.groups())
                self.date=datetime.datetime.strptime(date,'%Y %m %d %H:%M')
                self.url+="?date={}".format(self.date.strftime("%d+%B+%Y+%H:%M"))
        print("Navigation à {}".format(self.url))
        self.hint=QLabel("Retrouvez ces informations à : {}".format(
            self.url
        ))
        self.ui.statusbar.addPermanentWidget(self.hint)
        # on fixe la dimension de la fenêtre
        # avant d'y afficher quoi que ce soit
        # sinon les ajustements d'échelle sont inopérants.
        self.showFullScreen()
        ## on ajuste l'accès au web
        proxies=[]
        if parsed.proxy:
            print("Proxy utilisé : {}".format(parsed.proxy)  )
            proxies.append((parsed.proxy, parsed.port))
            proxies=proxies*3 # on essayera trois fois le premier proxy
        proxies.append((None,3128)) # solution de secours : pas de proxy
        for p in proxies:
            self.proxyHost=p[0]
            self.proxyPort=int(p[1])
            if self.loadURL()==QNetworkReply.NoError:
                # Si on arrive à obtenir un résultat par un des proxys
                # de la liste, on le garde et on stoppe les recherches.
                break
        self.loadURL()
        ## on programme la répétition de ce rechargement de l'url
        delay=20 * 1000                 ## toutes les 20 secondes
        self.timer=QTimer()
        atexit.register(self.stopTimer)
        self.timer.timeout.connect(self.loadURL)
        self.timer.start(delay)
        return

    def stopTimer(self):
        self.timer.stop()
        return

    def loadURL(self):
        """
        charge une URL dans le navigateur intégré. S'il y a une réponse,
        la page est affichée.
        @return l'objet erreur résultat. S'il vaut QNetworkReply.NoError c'est
          que tout va bien.
        """
        el=QEventLoop()
        mgr=QNetworkAccessManager()
        if self.proxyHost:
            proxy=QNetworkProxy(QNetworkProxy.HttpProxy,
                                hostName=self.proxyHost,
                                port=self.proxyPort)
            mgr.setProxy(proxy)
        mgr.finished.connect(el.quit)
        req=QNetworkRequest(QUrl(self.url))
        reply=mgr.get(req)
        el.exec_()

        error = reply.error()
        if error == QNetworkReply.NoError:
            self.ui.statusbar.showMessage("Mise à jour ... ok.", 1000)
            self.ui.webView.load(req)
        else:
            self.ui.statusbar.showMessage("Échec : {}".format(reply.errorString()), 1000)
        return error

def parse(args):
    """
    Prend en compte les arguments (à utiliser avec sys.argv[1:]
    @return un NameSpace avec les variables date, proxy, port et url
    positionnées
    """
    parser = argparse.ArgumentParser(
        prog='afficheur.py',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''\
  +-------------------------------------------------+
  | Affiche l'emploi du temps pour l'heure courante |
  +-------------------------------------------------+
     - l'affichage se fait en plein écran
     - on peut simuler une date et une heure (option --date)
     - on peut déclarer un proxy pour accéder au service EDT
  +-------------------------------------------------+
  |                      EXEMPLE                    |
  +-------------------------------------------------+
      afficheur.py http://edt.example.com/maintenant -d "2018 11 14 15:30" -x proxy.mondomaine.com
'''),
    )
    parser.add_argument('--date', '-d', help='Simule une date (exemple : "2018 11 14 09:30")', default="")
    parser.add_argument('--proxy', '-x', dest='proxy', help='Nom d\'hôte du proxy (exemple : "proxy.mondomaine.com")', default="")
    parser.add_argument('--port', '-p', help="Port du proxy (3128 par défaut)", default="3128")
    parser.add_argument('url', help='URL du service EDT (exemple : "http://edt.exemple.com/maintenant")')
    parsed=parser.parse_args(args)
    return parsed

def main(args) :
    parsed=parse(args[1:])
    app=QApplication(args)
    w=MaFenetre(parsed)
    w.showFullScreen()
    app.lastWindowClosed.connect(app.quit)
    app.exec_()
         
if __name__=="__main__":
    main(sys.argv)
    
