# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'afficheur.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(858, 644)
        MainWindow.setStatusTip("")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.toolButton = QtWidgets.QToolButton(self.centralwidget)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("exit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButton.setIcon(icon)
        self.toolButton.setObjectName("toolButton")
        self.horizontalLayout.addWidget(self.toolButton)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.webView = QtWebKitWidgets.QWebView(self.centralwidget)
        self.webView.setUrl(QtCore.QUrl("about:blank"))
        self.webView.setObjectName("webView")
        self.verticalLayout.addWidget(self.webView)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 858, 23))
        self.menubar.setObjectName("menubar")
        self.menuOptions = QtWidgets.QMenu(self.menubar)
        self.menuOptions.setObjectName("menuOptions")
        self.menu_propos = QtWidgets.QMenu(self.menubar)
        self.menu_propos.setObjectName("menu_propos")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionDate = QtWidgets.QAction(MainWindow)
        self.actionDate.setObjectName("actionDate")
        self.actionHeure = QtWidgets.QAction(MainWindow)
        self.actionHeure.setObjectName("actionHeure")
        self.actionLicence = QtWidgets.QAction(MainWindow)
        self.actionLicence.setObjectName("actionLicence")
        self.actionMode_d_emploi = QtWidgets.QAction(MainWindow)
        self.actionMode_d_emploi.setObjectName("actionMode_d_emploi")
        self.menuOptions.addAction(self.actionDate)
        self.menuOptions.addAction(self.actionHeure)
        self.menu_propos.addAction(self.actionLicence)
        self.menu_propos.addAction(self.actionMode_d_emploi)
        self.menubar.addAction(self.menuOptions.menuAction())
        self.menubar.addAction(self.menu_propos.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Date et heure"))
        self.toolButton.setText(_translate("MainWindow", "..."))
        self.toolButton.setShortcut(_translate("MainWindow", "Ctrl+Q"))
        self.menuOptions.setTitle(_translate("MainWindow", "Options"))
        self.menu_propos.setTitle(_translate("MainWindow", "&À propos"))
        self.actionDate.setText(_translate("MainWindow", "&Date ..."))
        self.actionHeure.setText(_translate("MainWindow", "&Heure ..."))
        self.actionLicence.setText(_translate("MainWindow", "&Licence ..."))
        self.actionMode_d_emploi.setText(_translate("MainWindow", "&Mode d\'emploi ..."))

from PyQt5 import QtWebKitWidgets
