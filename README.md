Gestion des salles de classe
============================


**edtpc** permet de gérer l'affectation de salles de classes aux
professeurs et aux élèves en physique-chimie, au lycée Jean Bart ;
ça pourrait être utile dans d'autres cas de figure.

Le moteur de l'application est un service Django, avec une base de
données Sqlite3. C'est une application web.

Mode d'emploi
=============

Le principe de fonctionnement est le suivant :

Création de structures
----------------------

Un administrateur de l'application définit un ensemble de salles,
un ensemble de professeurs et un ensemble de groupes d'élèves.

![interface d'administration](edt1.png)

Mise en place des étiquettes dans une grille chaque jour
--------------------------------------------------------

Chaque jour, on peut placer des étiquettes dans un grille
qui ressemble un peu aux antiques tableaux de fer où l'on plaçait
des étiquettes en té, pour gérer des plannings.

Les étiquettes peuvent être tirées-glissées d'une place à
l'autre de la grille, ou alors on peut les tirer-glisser vers
une poubelle, ou vers un "photocopieur" pour en créer de nouvelles.

![placement des étiquettes](edt2.svg)

Les repères sur l'image indiquent les principales opérations possibles.

- **0** choisir la date du jour (marqué option zéro, car par défaut
  c'est la date d'aujourd'hui). Quand on change cette date, on peut 
  accéder aux enregistrements d'autres jours, dans le passé ou dans 
  le futur.
- **A** les étiquettes ont une couleur qui est choisie à leur création,
  elles indiquent un professeur et un groupe d'élèves. Leur hauteur 
  signifie la durée d'occupation d'une salle (par incréments d'une demi-heure,
  c'est à dire selon la granularité du tableau). Chaque étiquette est mobile,
  le dplacement vertical change l'heure, le déplacement horizontal
  change la salle d'affectation. Les cases du tableau sont "aimantées" donc
  à la fin d'un tirer-glisser la salle et les heures sont précises. On peut
  aussi tirer une étiquette en dehors du tableau.
- **B** Un clic sur l'incône bleu clair enregistre les positions des
  étiquettes dans la base de données, à la date qui est marquée 
  juste dessous. Si on veut recopier "une journée" vers une autre, il
  suffit que la date de sauvegarde soit différente de la date des données
  lues (en zéro).
- **C** Photocopieur : si on tire-glisse une étiquette sur cette icone,
  on peut la dupliquer telle quelle, ou en modifiant quelques-un de ses
  attributs : couleur, durée, nom du prof ou du groupe d'élèves. C'est
  la méthode la plus facile pour créer de nouvelle étiquettes mobiles.
  L'ancienne étiquette revient à sa place sans modification lors 
  d'une opération de copie, la nouvelle est créée dans une zone qui échappe
  à l'aimantation, il ne reste plus qu'à la glisser vers le tableau aimanté.
- **D** Poubelle : quand on tire-glisse une étiquette sur cette icone,
  l'étiquette est supprimée.
  
Consulation par les étudiants
=============================

En l'absence d'authentification, tout la monde a accès à l'url
https://nom-du-site.web//maintenant , où est servie une vue dynamique,
où ne figurent que les salles occupées, pendant une durée qui englobe
l'heure présente, si bien qu'on peu savoir, quand on est étudiant, où
trouver tel ou tel professeur dans sa salle.

![affichage "maintenant"](edt3.png)
