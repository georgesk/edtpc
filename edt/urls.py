"""edt URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import etiquettes.views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^$', etiquettes.views.index, name="index"),
    url(r'^adminEdt$', etiquettes.views.adminEdt, name="adminEdt"),
    url(r'^cree_etiquette$', etiquettes.views.cree_etiquette, name="cree_etiquette"),
    url(r'^save$', etiquettes.views.save, name="save"),
    url(r'^maintenant.*$', etiquettes.views.maintenant, name="maintenant"),
    url(r'^importer$', etiquettes.views.importer, name="importer"),
    url(r'^ping$', etiquettes.views.ping, name="ping"),
    url(r'^css/edt-calcule.css$', etiquettes.views.cssCalcule, name="edt-calcule.css"),
    url(r'^js/functions.js$', etiquettes.views.jsCalcule, name="functions.js"),
]
